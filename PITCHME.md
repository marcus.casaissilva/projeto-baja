---?image=assets/img/BAJA-SAE1.jpg
# Programa Baja SAE BRASIL

---
## Baja SAE BRASIL

O programa Baja SAE BRASIL é um desafio lançado a estudantes que oferece a oportunidade de aplicar na prática, os conhecimentos adiquiridos academicamente, participando de um caso real de desenvolvimento de um veículo off-road desde sua concepção, projeto detalhado, construção e testes. Visando a preparação para o mercado de trabalho e a real vivêcia no desenvolvimento de um projeto.

---
## Equipes

As equipes representam as universidades a que os alunos integrantes estão vinculados e são estimulados a participarem anualmente da competição, que avalia comparativamente os projetos, sendo as equipes vencedoras convidadas a participar da competição internacional.

---
## Premissas do Projeto

O projeto consiste na construção de um veículo monoposto, off-road, esportivo, o qual deve ser um protótipo para produção em série, confiavel e com produção estimada em 4000 unidades por ano. 
Devendo a equipe dirigir e trabalhar em todas as fases do projeto, bem como no desenvolvimento de toda documentação associada, e cabendo aos mesmos viabilizar o suporte financeiro para a realização do mesmo.

---
## Requisitos do projeto

O veículo deve ter 4 ou mais rodas, utilizar um motor Briggs  & Stratton especificado no regulamento, acomodar condutores de todos os portes, desde o percentil masculino 99% ao percentil feminino 1%, em posição confortavel e atendendo os requisitos de segurança, ter ao menos duas chaves gerais, que devem desativao o funcionamento do motor, uma luz de freio e uma bateria.

---
## Baja FMF Wyden

A equipe é formada pelos seguintes integrantes:
- Marcus Paulo C. da Silva - Eng. Mecânica
- João Paulo T. da S. de Aquino - Eng. Mecânica
- Raquel Falcão Luasses - Eng. Elétrica
- Francisco C. M. Neto - Eng. Mecânica
- José Roberto da S. Pantoja - Eng. Mecânica
- José Vitor R. da Silva - Eng. Mecânica
- Alexandre Ícaro Fonseca - Eng. Elétrica

@snap[south span-100 text-04]
Alguns interessados ainda não confirmados não constam na lista.
@snapend

---
## Custos previstos

Com base em analises de projetos de equipes já participantes da competição, acredita-se que os custos de produção do protótipo girem em torno de 30.000 à 45.000.

@snap[text-04]
["ANÁLISE E FORMULAÇÃO DOS GASTOS PARAFABRICAÇÃO DE UM VEÍCULO BAJA NO CENTRO UNIVERSITÁRIO UNIVATES", Monografia de Augusto César Castro, 2016. @fa[external-link]](https://www.univates.br/bdu/bitstream/10737/1255/1/2016AugustoCesarCastro.pdf)
@snapend
@snap[text-04]
["LEVANTAMENTO E CONTROLE DE CUSTOS DE MATERIAIS DE UM VEÍCULO DE COMPETIÇÃO OFF-ROAD BAJA", TCC de Deise Kich, 2015. @fa[external-link]](https://repositorio.unisc.br/jspui/bitstream/11624/1028/1/Deise%20Kich.pdf)
@snapend


---
## Espaço e ferramental nescessário

Para o correto desenvolvimento da fabricação e montagem do projeto nescessitaremos de uma oficina com espaço e o ferramental mínimo para o mesmo, como; chaves, ferramentas de corte, solda e dobra de aço, ferramentas para trabalho com circuitos elétricos, dentre outras.
